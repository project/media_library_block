<?php

declare(strict_types=1);

namespace Drupal\Tests\media_library_block\Kernel;

use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\Tests\media\Traits\MediaTypeCreationTrait;

/**
 * Tests deriving of MediaLibraryBlock.
 *
 * @group media_library_block
 */
class MediaLibraryBlockDeriverTest extends EntityKernelTestBase {

  use MediaTypeCreationTrait;

  /**
   * The block manager.
   *
   * @var \Drupal\Core\Block\BlockManagerInterface
   */
  protected $blockManager;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'file',
    'media',
    'image',
    'media_library_block',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->installSchema('file', ['file_usage']);
    $this->installEntitySchema('file');
    $this->installEntitySchema('media');
    $this->createMediaType('image', ['id' => 'image', 'label' => 'Image']);
    $this->createMediaType('image', ['id' => 'document', 'label' => 'Document']);
    $this->createMediaType('image', ['id' => 'teaser', 'label' => 'Teaser']);
    $this->blockManager = $this->container->get('plugin.manager.block');
  }

  /**
   * Tests the derivative definitions.
   */
  public function testDerivative(): void {
    $definitions = $this->blockManager->getDefinitions();

    $this->assertArrayHasKey('media_library_block:document', $definitions);
    $this->assertArrayHasKey('media_library_block:image', $definitions);
    $this->assertArrayHasKey('media_library_block:teaser', $definitions);
    $this->assertEquals('Document', $definitions['media_library_block:document']['admin_label']);
  }

}
