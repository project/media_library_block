<?php

declare(strict_types=1);

namespace Drupal\media_library_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\media\MediaInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a media browser block.
 *
 * @Block(
 *   id = "media_library_block",
 *   admin_label = @Translation("Media Library Block"),
 *   category = @Translation("Media"),
 *   deriver = "Drupal\media_library_block\Plugin\Derivative\MediaLibraryBlockDeriver"
 * )
 */
class MediaLibraryBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Media bundle allowed for selecting.
   *
   * @var string
   */
  protected $allowedBundle;

  /**
   * Entity display repository.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->entityDisplayRepository = $container->get('entity_display.repository');
    $instance->allowedBundle = $instance->getDerivativeId();
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'media' => '',
      'view_mode' => 'default',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $form['media'] = [
      '#type' => 'media_library',
      '#allowed_bundles' => [$this->allowedBundle],
      '#title' => $this->t('Media'),
      '#default_value' => $this->configuration['media'] ?? NULL,
      '#required' => TRUE,
      '#cardinality' => 1,
      '#description' => $this->t('Add or select media.'),
    ];

    $options = $this->entityDisplayRepository->getViewModeOptionsByBundle('media', $this->allowedBundle);

    $form['view_mode'] = [
      '#type' => 'select',
      '#title' => $this->t('View mode'),
      '#options' => $options,
      '#default_value' => $this->configuration['view_mode'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);

    // The media_library form returns the selected media as e.g. '3,6,12'.
    // The form element allows selecting multiple media, even when setting
    // cardinality to 1. So only take the first.
    $value = $form_state->getValue('media') ?? '';
    $media = explode(',', $value)[0] ?? '';
    $this->configuration['media'] = $media;

    // View mode.
    $view_mode = $form_state->getValue('view_mode') ?? 'default';
    $this->configuration['view_mode'] = $view_mode;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];

    if ($media = $this->loadMedia()) {
      $access = $media->access('view', NULL, TRUE);
      if ($access->isAllowed()) {
        $view_builder = $this->entityTypeManager->getViewBuilder('media');
        $view_mode = $this->getViewmode();
        $build = $view_builder->view($media, $view_mode);
      }
      CacheableMetadata::createFromObject($media)
        ->merge(CacheableMetadata::createFromRenderArray($build))
        ->merge(CacheableMetadata::createFromObject($access))
        ->applyTo($build);
    }
    return $build;
  }

  /**
   * Load and return the referenced media.
   *
   * @return \Drupal\media\MediaInterface|null
   *   The loaded media, if available.
   */
  public function loadMedia(): ?MediaInterface {
    if ($this->configuration['media']) {
      /** @var \Drupal\media\MediaInterface $entity */
      $entity = $this->entityTypeManager->getStorage('media')->load($this->configuration['media']);
      return $entity;
    }
    return NULL;
  }

  /**
   * Return the selected viewmode for the media.
   *
   * @return string
   *   The viewmode.
   */
  public function getViewmode(): string {
    return $this->configuration['view_mode'] ?? 'full';
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    $dependencies = parent::calculateDependencies();
    if ($media = $this->loadMedia()) {
      $dependencies[$media->getConfigDependencyKey()][] = $media->getConfigDependencyName();
    }
    $dependencies['config'][] = 'core.entity_view_display.media.' . $this->allowedBundle . '.' . $this->getViewmode();
    $dependencies['config'][] = 'media.type.' . $this->allowedBundle;
    return $dependencies;
  }

}
